# SARS-CoV-2 PCR training


*************************************
********* english version *********
*************************************
How to use these documents for remote training on SARS-CoV-2 PCR diagnostic

All documents in this repository are distributed under the CC-BY-4.0 licence.

These documents were originally produced for participants of an remote training programme for SARS-CoV-2 PCR diagnostic provided by the Robert Koch Institute in April 2020. All documents are produced in a way to allow experienced users to independently work their way through it, which is why they are provided here.
Refer to the General_remarks.pdf file for further instructions on how to use this material.

Disclaimer:
The authors made every effort to provide accurate information. Nevertheless, mistakes may occur. The authors do not assume liability for relevance, accuracy and completeness of the information provided.



*************************************
********* version française *********
*************************************
Comment utiliser ces documents pour la formation à distance sur le diagnostic par PCR du SRAS-CoV-2

Tous les fichiers de ce dossier sont distribués sous la licence CC-BY-4.0.

Ces documents et films ont été produits à l'origine pour les participants d'un programme de formation à distance au diagnostic par PCR du SARS-CoV-2 fourni par l'Institut Robert Koch en avril 2020.  Cependant, de nombreux fichiers peuvent être appliqués à différents contextes et tous les fichiers ont été produits de manière à permettre aux utilisateurs expérimentés de s'y retrouver de manière indépendante. 
Veuillez vous référer au fichier General_remarks.pdf pour de plus amples instructions sur la façon d'utiliser ce matériel.

Avertissements:
Les auteurs se sont efforcés de fournir des informations précises. Néanmoins, des erreurs peuvent se produire. Les auteurs n'assument aucune responsabilité quant à la pertinence, l'exactitude et l'exhaustivité des informations fournies. 
Pour les informations relatives à la sécurité des différents composants, veuillez-vous référer au manuel du fabricant.



************************************
********* version español *********
************************************
Cómo utilizar estos documentos para la capacitación a distancia en el diagnóstico de SARS-CoV-2 mediante PCR

Todos los archivos de este depósito se distribuyen bajo la licencia CC-BY-4.0.

Estos documentos y películas fueron producidos originalmente para los participantes del programa de capacitación a distancia para el diagnóstico de PCR del SARS-CoV-2 proporcionado por el Instituto Robert Koch en abril del 2020.  Sin embargo, muchos de los documentos pueden aplicarse a diferentes escenarios y todos han sido producidos de manera que los usuarios experimentados puedan trabajar de forma independiente.  No todos los documentos se encuentran disponibles en espanõl, sin embargo agregaremos más documentos en los proximos días.
Consulte el archivo General_remarks.pdf para obtener más instrucciones sobre cómo utilizar este material.

Advertencia:
Los autores se han esforzado por proporcionar información precisa. No obstante, pueden producirse errores. Los autores no asumen ninguna responsabilidad por la pertinencia, exactitud e integridad de la información proporcionada. 
Para información sobre la seguridad de los componentes individuales, por favor, consulte el manual del fabricante.
